package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yettyzzzone.demo_3colorsstudio.R;

public class Search_Finish extends AppCompatActivity implements View.OnClickListener{

    TextView textViewMyAddress, textViewMyAddress2, textViewMyAddress3;
    Button buttonMyAddress1, buttonMyAddress2, buttonMyAddress3;

    public static final String LOCATION_FINISH = "com.yettyzzzone.demo_3colorsstudio.locationFinish";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__finish);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewMyAddress = (TextView)findViewById(R.id.textViewMyAddress);
        textViewMyAddress2 = (TextView)findViewById(R.id.textViewMyAddress2);
        textViewMyAddress3 = (TextView)findViewById(R.id.textViewMyAddress3);

        buttonMyAddress1 = (Button)findViewById(R.id.buttonMyAddress);
        buttonMyAddress2 = (Button)findViewById(R.id.buttonMyAddress2);
        buttonMyAddress3 = (Button)findViewById(R.id.buttonMyAddress3);

        buttonMyAddress1.setOnClickListener(this);
        buttonMyAddress2.setOnClickListener(this);
        buttonMyAddress3.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        Intent intent = new Intent();
        String location;
        switch (v.getId()){
            case R.id.buttonMyAddress:
                location = textViewMyAddress.getText().toString();
                intent.putExtra(LOCATION_FINISH,location);
                break;
            case R.id.buttonMyAddress2:
                location = textViewMyAddress2.getText().toString();
                intent.putExtra(LOCATION_FINISH,location);
                break;
            case R.id.buttonMyAddress3:
                location = textViewMyAddress3.getText().toString();
                intent.putExtra(LOCATION_FINISH,location);
                break;
        }
        setResult(RESULT_OK,intent);
        finish();
    }
}
