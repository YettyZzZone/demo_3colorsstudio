package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.yettyzzzone.demo_3colorsstudio.R;

public class Comment extends AppCompatActivity {
    Button btnSend;
    EditText editText;
    public static final String COMMENT = "com.yettyzzzone.demo_3color.comment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editText = (EditText)findViewById(R.id.editTextComment);
        btnSend = (Button)findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentComment = new Intent();
                String comment = editText.getText().toString();
                intentComment.putExtra(COMMENT,comment);
                setResult(RESULT_OK,intentComment);
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
