package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.yettyzzzone.demo_3colorsstudio.R;


public class OrderTaxi extends AppCompatActivity implements View.OnClickListener{
    ImageButton btnEconom, btnComfort, btnBisnes, btnMinivan;
    Button btnTime, btnComment, btnFinish;
    TextView tariff, textViewTime,textViewComment, textViewFinish;
    Spinner spinner;

    public static final int TIME = 1;
    public static final int COMMENT = 2;
    public static final int SEARCH_FINISH = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_taxi);


        btnEconom = (ImageButton)findViewById(R.id.btnEconomOrder);
        btnComfort = (ImageButton)findViewById(R.id.btnComfortOrder);
        btnBisnes = (ImageButton)findViewById(R.id.btnBisnesOrder);
        btnMinivan = (ImageButton)findViewById(R.id.btnMinivanOrder);

        btnTime = (Button)findViewById(R.id.buttonTime);
        btnTime.setOnClickListener(this);
        btnComment = (Button)findViewById(R.id.buttonComment);
        btnComment.setOnClickListener(this);
        btnFinish = (Button)findViewById(R.id.buttonFinish);
        btnFinish.setOnClickListener(this);

        tariff = (TextView)findViewById(R.id.tariff);
        textViewTime = (TextView)findViewById(R.id.textViewTime);
        textViewComment = (TextView)findViewById(R.id.textViewComment);
        textViewFinish = (TextView)findViewById(R.id.textViewFinish);

        spinner = (Spinner)findViewById(R.id.spinner);

        //Передача адреса
        Button btnAddress = (Button)findViewById(R.id.buttonAddress);

        Intent intent = getIntent();
        String s = intent.getStringExtra(MainActivity.ADDRESS);
        btnAddress.setText(s);

        Select();
        Spinner();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonTime:
                Intent intentTime = new Intent(getApplicationContext(),Time.class);
                startActivityForResult(intentTime,TIME);
                break;
            case R.id.buttonComment:
                Intent intentComment = new Intent(getApplicationContext(),Comment.class);
                startActivityForResult(intentComment,COMMENT);
                break;
            case R.id.buttonFinish:
                Intent intentSearch = new Intent(getApplicationContext(),Search_Finish.class);
                startActivityForResult(intentSearch,SEARCH_FINISH);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==TIME){
            if(resultCode==RESULT_OK){
                String time = data.getStringExtra(Time.RESULT);
                textViewTime.setText(time);
            }
        }
        if(requestCode==COMMENT){
            if(resultCode==RESULT_OK){
                String comment = data.getStringExtra(Comment.COMMENT);
                textViewComment.setText(comment);
            }
        }
        if(requestCode==SEARCH_FINISH){
            if(resultCode==RESULT_OK){
                String finish = data.getStringExtra(Search_Finish.LOCATION_FINISH);
                textViewFinish.setText(finish);
            }
        }
    }
    //Выбор тарифа
    private void Select(){
        int def = 0;
        Intent extras = getIntent();
        int taxi_select = extras.getIntExtra(MainActivity.TAXI_SELECT, def);
        switch (taxi_select){
            case 1:
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnEconom.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_econom);
                break;
            default:
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnComfort.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_comfort);
                break;
            case 3:
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_bisnes);
                break;
            case 4:
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_minivan);
                break;
        }
    }
    //Выбор подъезда
    private void Spinner(){
        Intent intent = getIntent();
        int item = intent.getIntExtra(MainActivity.SPINNER, 1);
        spinner.setSelection(item);
    }
}
