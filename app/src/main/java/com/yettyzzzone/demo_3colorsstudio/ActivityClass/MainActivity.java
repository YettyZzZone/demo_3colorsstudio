package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yettyzzzone.demo_3colorsstudio.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnMapReadyCallback {

    GoogleMap mMap;
    ImageButton btnEconom, btnComfort, btnBisnes, btnMinivan;
    Button btnConfirm, buttonAddress;
    TextView tariff;
    Intent intentOrder;
    Spinner spinner;
    String address;

    public static final String TAG = "myLogs";
    public static final String TAXI_SELECT = "com.yettyzzzone.demo_3colorsstudio.taxi_select";
    public static final String ADDRESS = "com.yettyzzzone.demo_3colorsstudio.address";
    public static final String SPINNER = "com.yettyzzzone.demo_3colorsstudio.spinner";

    public static final int LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add buttons
        btnEconom = (ImageButton)findViewById(R.id.btnEconom);
        btnComfort = (ImageButton)findViewById(R.id.btnComfort);
        btnBisnes = (ImageButton)findViewById(R.id.btnBisnes);
        btnMinivan = (ImageButton)findViewById(R.id.btnMinivan);
        tariff = (TextView)findViewById(R.id.tariff);
        btnConfirm = (Button)findViewById(R.id.buttonConfirm);
        buttonAddress = (Button)findViewById(R.id.buttonAddress);
        intentOrder = new Intent(this,OrderTaxi.class);


        //OnClick
        btnEconom.setOnClickListener(this);
        btnComfort.setOnClickListener(this);
        btnBisnes.setOnClickListener(this);
        btnMinivan.setOnClickListener(this);
        buttonAddress.setOnClickListener(buttonAdressOnClick);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Создаем карту и добавляем маркер
        createMapView();
        //Определяем тариф по умолчанию
        Default();

        //Кнопка заказать
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentOrder.putExtra(ADDRESS, address);
                Spinner();
                startActivity(intentOrder);
            }
        });
    }

    //Включаем отображение карты
    private void createMapView() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onClick(View v) {
        int econom = 1, comfort = 2, bisnes = 3, minivan = 4;
        switch (v.getId()){
            case R.id.btnEconom:
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnEconom.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_econom);
                intentOrder.putExtra(TAXI_SELECT, econom);
                break;
            case R.id.btnComfort:
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnComfort.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_comfort);
                intentOrder.putExtra(TAXI_SELECT, comfort);
                break;
            case R.id.btnBisnes:
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_bisnes);
                intentOrder.putExtra(TAXI_SELECT, bisnes);
                break;
            case R.id.btnMinivan:
                btnComfort.setBackgroundColor(Color.TRANSPARENT);
                btnBisnes.setBackgroundColor(Color.TRANSPARENT);
                btnEconom.setBackgroundColor(Color.TRANSPARENT);
                btnMinivan.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
                tariff.setText(R.string.seat_minivan);
                intentOrder.putExtra(TAXI_SELECT, minivan);
                break;
        }
    }
    private void Default() {
        btnComfort.setBackground(getResources().getDrawable(R.drawable.tariff_galochka));
        tariff.setText(R.string.seat_comfort);
        intentOrder.putExtra(TAXI_SELECT, 2);
    }
    private void Spinner() {
        spinner = (Spinner) findViewById(R.id.spinner);
        int selected = spinner.getSelectedItemPosition();
        intentOrder.putExtra(SPINNER, selected);
    }
    View.OnClickListener buttonAdressOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),Search.class);
            startActivityForResult(intent,LOCATION);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==LOCATION){
            if(resultCode==RESULT_OK){
                buttonAddress.setText(data.getStringExtra(Search.LOCATION));
            }
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.order_taxi) {
            // Handle the camera action
        } else if (id == R.id.info_taxi) {

        } else if (id == R.id.profile) {

        } else if (id == R.id.my_order) {

        } else if (id == R.id.about) {

        } else if (id == R.id.exit) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Считываем координаты
        Double latitude = getIntent().getExtras().getDouble("latitude");
        Double longitude = getIntent().getExtras().getDouble("longitude");

        //Определяем адрес
        try {
            Geocoder geo = new Geocoder(MainActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
            if(addresses.isEmpty()){
                buttonAddress.setText("Ваше местоположение не известно");
            }
            else{
                if(addresses.size() > 0){
                    address = addresses.get(0).getAddressLine(0);
                    buttonAddress.setText(address);
                }
            }
        } catch (IOException e) {
            Log.d(TAG, e.toString());
        }

        //Записываем координаты для маркера
        LatLng ll = new LatLng(latitude, longitude);
        float zoom = 15f;

        //Зуммируем карту
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(ll, zoom);

        //Добавляем маркер на карту
        mMap.addMarker((new MarkerOptions()
                .position(ll)
                .title(getResources().getString(R.string.marker))
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.i_am_map))));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
        mMap.animateCamera(cameraUpdate);
    }
}
