package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yettyzzzone.demo_3colorsstudio.R;

public class Search extends AppCompatActivity implements View.OnClickListener{
    public static final String LOCATION = "com.yettyzzzone.demo_3colorsstudio.location";

    TextView textViewArround1, textViewArround2, textViewArround3;
    Button buttonArround1, buttonArround2, buttonArround3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewArround1 = (TextView)findViewById(R.id.textViewArround1);
        textViewArround2 = (TextView)findViewById(R.id.textViewArround2);
        textViewArround3 = (TextView)findViewById(R.id.textViewArround3);

        buttonArround1 = (Button)findViewById(R.id.buttonArround1);
        buttonArround2 = (Button)findViewById(R.id.buttonArround2);
        buttonArround3 = (Button)findViewById(R.id.buttonArround3);

        buttonArround1.setOnClickListener(this);
        buttonArround2.setOnClickListener(this);
        buttonArround3.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        String location;
        switch (v.getId()){
            case R.id.buttonArround1:
                location = textViewArround1.getText().toString();
                intent.putExtra(LOCATION,location);
                break;
            case R.id.buttonArround2:
                location = textViewArround2.getText().toString();
                intent.putExtra(LOCATION,location);
                break;
            case R.id.buttonArround3:
                location = textViewArround3.getText().toString();
                intent.putExtra(LOCATION,location);
                break;
        }
        setResult(RESULT_OK,intent);
        finish();
    }
}
