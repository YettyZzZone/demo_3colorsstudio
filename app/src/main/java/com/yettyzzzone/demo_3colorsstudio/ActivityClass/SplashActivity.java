package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.yettyzzzone.demo_3colorsstudio.R;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        //Создаем Handler с открытием следующего Activity через 2 секунды
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), SearchSplashActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2 * 1000);

    }
}
