package com.yettyzzzone.demo_3colorsstudio.ActivityClass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.yettyzzzone.demo_3colorsstudio.R;

public class Time extends AppCompatActivity {
    RadioButton time1, time2, time3, time4;
    RadioGroup radioGroup;

    public static final String RESULT = "com.yettyzzzone.demo_3colorstudio.result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        time1 = (RadioButton) findViewById(R.id.time1);
        time2 = (RadioButton) findViewById(R.id.time2);
        time3 = (RadioButton) findViewById(R.id.time3);
        time4 = (RadioButton) findViewById(R.id.time4);
    }

    public void radioClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.time1:
                intent.putExtra(RESULT, getResources().getString(R.string.time1));
                break;
            case R.id.time2:
                intent.putExtra(RESULT, getResources().getString(R.string.time2));
                break;
            case R.id.time3:
                intent.putExtra(RESULT, getResources().getString(R.string.time3));
                break;
            case R.id.time4:
                intent.putExtra(RESULT, getResources().getString(R.string.time4));
                break;
        }
        setResult(RESULT_OK,intent);
        finish();
    }
}
